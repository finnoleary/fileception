build:
	nasm -f bin src/main.nasm -o hello.elf32
	cp -a hello.elf32 hello.pdf
	dd if=src/test.pdf of=hello.pdf obs=1 conv=notrunc seek=132

package:
	zip -r code.zip .
	cat hello.pdf code.zip >> code+hello.pdf
	chmod +x code+hello.pdf
